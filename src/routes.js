import React from 'react';

// css imports
import HomeScreen from './screens/home';
import SignupScreen from './screens/signup';
import LoginScreen from './screens/signin';
import DashboardScreen from './screens/dashboard';
import ProjectdetailScreen from './screens/project_detail';
import ProjectmodelScreen from './screens/project_model';
import SchemaScreen from './screens/schema';
import ControllerScreen from './screens/controller';
import WorkflowScreen from './screens/workflow';
import WorkflowschemaScreen from './screens/workflow_schema';
import ControllersecurityScreen from './screens/controller_security';
import TestScreen from './screens/test';
import PermissionScreen from './screens/permission';
import {Route, BrowserRouter as Router} from 'react-router-dom';


class RouteComponent extends React.Component{
    render(){
        return(
            <Router>
                <Route exact path='/dashboard' component={DashboardScreen}/>
                <Route exact path='/' component = {HomeScreen}/>
                <Route exact path='/signup' component = {SignupScreen}/>
                <Route exact path='/login' component = {LoginScreen}/>
                <Route exact path='/projects/:projectname/home' component={ProjectdetailScreen}/>
                <Route exact path='/projects/:projectname/permissions' component={PermissionScreen}/>
                <Route exact path='/projects/:projectname/models' component={ProjectmodelScreen}/>
                <Route exact path='/projects/:projectname/controllers' component={ControllerScreen}/>
                <Route exact path='/projects/:projectname/models/:mname' component={SchemaScreen}/>
                <Route exact path='/projects/:projectname/controllers/:cname' component={WorkflowScreen}/>
                <Route exact path='/projects/:projectname/controllers/:cname/security' component={ControllersecurityScreen}/>
                <Route exact path='/projects/:projectname/controllers/:cname/methods/:option' component={WorkflowschemaScreen}/>
                <Route exact path='/test' component={TestScreen}/>
            </Router>
        );
    }
}


export default RouteComponent;
import React from 'react';

// css imports
import '../css/home.css';
import table from '../pics/table.png';


class ModelstepperComponent extends React.Component{
    render(){
        return(
            <div className="top_header">
                <div className="content_card">
                    <div className="head_content">
                        Design schema and API Assistant creates model for you.
                    </div>
                    <div className="description_content">
                        Design your schema and convert to relational table with single click. API Assistant
                        automatically creates highly scalable database instance and runs query for you under the hood.
                        
                    </div>
                </div>
                <div className="img_card">
                    <img src={table} alt="hello"></img>
                </div>
            </div>
        );
    }
}

export default ModelstepperComponent;
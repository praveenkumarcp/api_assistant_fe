import React from 'react';
import axios from 'axios';

// custom imports
import '../css/dashboard.css';

class NewmodelComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {title:''}
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleValueChange = this.handleValueChange.bind(this);
    }
    handleSubmit(e){
        e.preventDefault();
        var json_body = {table: this.state.title};
        axios.post(`http://localhost:4000/api/v1/projects/${this.props.title}/models`, json_body, {
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        })
        .then(response=>console.log(response.data))
        .catch(err=>console.log(err));
        this.setState({title:''});
    }
    handleValueChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    };
    render(){
        return(
            <div className="new_project_div">
                New Model
                <br></br>
                <form>
                <input value={this.state.title} onChange={this.handleValueChange} name="title" placeholder="Title" autoFocus autoComplete="off"/>
                <button onClick={this.handleSubmit}>CREATE</button>
                </form>
            </div>
        );
    }
}

export default NewmodelComponent;
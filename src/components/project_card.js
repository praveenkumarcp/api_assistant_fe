import React from 'react';
import {withRouter} from 'react-router-dom';

// css imports
import '../css/dashboard.css';

class ProjectcardComponent extends React.Component{
 
    render(){
        return(
            <div onClick={()=>{
                this.props.history.push(`/projects/${this.props.title}/home`)
            }} className="project_card">
                <i class="material-icons">folder</i>
                <center>
                    {this.props.title}
                </center>
            </div>
        )
    }
}

export default withRouter(ProjectcardComponent);
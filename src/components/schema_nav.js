import React from 'react';
import {withRouter} from 'react-router-dom';

import '../css/schema.css';

class SchemanavComponent extends React.Component{
  
    render(){
        return(
            <div>
                <br></br>
                 <hr></hr>
                <div className="schema_nav">
                    <div className="schema_nav_back">
                        <i onClick={()=>{
                            this.props.history.goBack();
                        }} class="material-icons">arrow_back</i>
                    </div>
                    <div className="schema_nav_left_header">
                        Project : {this.props.pname} 
                    </div>
                    <div className="schema_nav_right_header">
                        Model: {this.props.mname} 
                    </div>
                    
                </div>
            </div>
        );
    }
}

export default withRouter(SchemanavComponent);
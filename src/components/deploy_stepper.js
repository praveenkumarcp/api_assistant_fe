import React from 'react';

// css imports
import '../css/home.css';
import deploy from '../pics/serve.png';

class DeploystepperComponent extends React.Component{
    render(){
        return(
            <div className="top_header">
                <div className="content_card">
                    <div className="head_content">
                        Go live with a single tap
                    </div>
                    <div className="description_content">
                        API Assistant deploys your app on highly scalable server with simple clicks.
                        Deliver your mobile and web applications with high reliability and feel the 
                        fastest request-response cycle ever.
                    </div>
                </div>
                <div className="img_card">
                    <img src={deploy} alt="hello"/>
                </div>
            </div>
        );
    }
}

export default DeploystepperComponent;
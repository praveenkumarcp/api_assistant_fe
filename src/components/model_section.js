import React from 'react';

// custom imports
import '../css/projects.css';
import NewmodelComponent from '../components/newModel';
import ModellistComponent from './model_list';

class ModelsectionComponent extends React.Component{
    render(){
        return(
            <div className="model_section_div">
                <NewmodelComponent title={this.props.title}/>
                <ModellistComponent title={this.props.title}/>
            </div>
        )
    }
}

export default ModelsectionComponent;
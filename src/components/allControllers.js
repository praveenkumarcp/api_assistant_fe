import React from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';

import '../css/dashboard.css';
import '../css/projects.css';

class AllcontrollersComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {all_controllers: []};
    }
    async componentDidMount(){
        const url = `http://localhost:4000/api/v1/controllers/${this.props.pname}/controllers`;
        var response= await axios.get(url, {
            headers:{
                'Authorization': localStorage.getItem('token')
            }
        });
        console.log(response.data);
        var all_controllers =[];
        for(var i=0;i<response.data.controllers.length;i++){
            all_controllers.push(this.createCards(response.data.controllers[i]));
        }
        this.setState({all_controllers:all_controllers});
    }
    createCards = (cname)=>{
        return(
            <div onClick={()=>
            {this.props.history.push(`/projects/${this.props.pname}/controllers/${cname}`)}
            } className="controller_card">
                <i style={{fontWeight:'lighter'}} class="material-icons">developer_board</i>
                <center>
                    <div>{cname}</div>
                </center>
            </div>
        );
    }
    render(){
        return(
            <div className="model_section_div">
            <div className="project_list_div">
                {this.props.pname} project controllers
                <div className="project_list_section">
                    {this.state.all_controllers}
                </div>
            </div>
            </div>
        );
    }
}

export default withRouter(AllcontrollersComponent);
import React from 'react';

// css imports
import '../css/home.css';

import server from '../pics/header1.jpg.jpg';

class IntrostepperComponent extends React.Component{
    render(){
        return(
            <div className="top_header">
                <div className="content_card">
                    <div className="head_content">
                        Model. Customize. Host
                        <br></br>
                        APIs on the fly.
                    </div>
                    <div className="description_content">
                        API Assistant is a platform to create REST APIs using highly customizable models
                        and workflows with drag and drop feature.
                        Go live in seconds with a simple tap and access your APIs across the planet.
                    </div>
                </div>
                <div className="img_card">
                    <img src={server} alt="hello"></img>
                </div>
            </div>
        );
    }
}

export default IntrostepperComponent;
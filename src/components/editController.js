import React from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';

import '../css/controller.css';
import '../css/dashboard.css';

class EditControllerComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {endpoint:'', cname:'', del_dis:'none'};
    }
    handleValueChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    };
    handleSubmit = async(e)=>{
        e.preventDefault();
        var url = `http://localhost:4000/api/v1/controllers/${this.props.pname}/controllers`;
        axios.post(url, this.state, {
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        })
        .then(response=>console.log(response.data))
        .catch(err=>console.log(err));
        this.setState({cname:'', endpoint:''});
    }
    postWidget = ()=>{
        return(
            <center>
            <table>
                <tr>
                    <td><i class="material-icons">save</i></td>
                    <td>POST</td>
                </tr>
            </table>
            </center>
        );
    }
    editWidget = ()=>{
        return(
            <center>
            <table>
                <tr>
                    <td><i class="material-icons">edit</i></td>
                    <td>EDIT</td>
                </tr>
            </table>
            </center>
        );
    }
    getWidget = ()=>{
        return(
            <center>
            <table>
                <tr>
                    <td><i class="material-icons">get_app</i></td>
                    <td>GET</td>
                </tr>
            </table>
            </center>
        );
    }
    deleteWidget = ()=>{
        return(
            <center>
            <table>
                <tr>
                    <td><i class="material-icons">delete</i></td>
                    <td>DELETE</td>
                </tr>
            </table>
            </center>
        );
    }
    routePage = (method)=>{
        this.props.history.push(`/projects/${this.props.pname}/controllers/${this.props.cname}/methods/${method}`);
    }
    render(){
        return(
            <div>
                <div className="new_controller">
                    Edit Controller
                    <br></br>
                    <form>
                    <input name="cname" onChange={this.handleValueChange} value={this.state.cname} placeholder="Title" size={13} autoComplete="off" autoFocus></input>
                    <input style={{marginLeft:'20px'}} name="endpoint" onChange={this.handleValueChange} value={this.state.endpoint} placeholder="Endpoint " size={50} autoComplete="off"></input>
                    <button onClick={this.handleSubmit}>CHANGE</button>
                    </form>
                </div>
                <div className="controller_method_div">
                    HTTP Methods
                    <br></br>
                    <div onClick={()=>{this.routePage('post')}} className="method_div">{this.postWidget()}</div>
                    <div onClick={()=>{this.routePage('get')}}  className="method_div">{this.getWidget()}</div>
                    <div onClick={()=>{this.routePage('put')}}  className="method_div">{this.editWidget()}</div>
                    <div onClick={()=>{this.routePage('delete')}}  className="method_div">{this.deleteWidget()}</div>    
                </div>
                <div className="delete_div">
                    Delete
                    <table>
                        <tr style={{cursor:'grab'}} onClick={()=>{this.setState({del_dis: this.state.del_dis === 'block' ? 'none':'block'})}}>
                            <td>
                            <i class="material-icons">delete_forever</i>
                            </td>
                            <td>Delete this controller</td>
                        </tr>
                    </table>
                    <div style={{display:this.state.del_dis}} className="del_msg_div">
                        Are you sure to delete the controller?
                        <br></br>
                        <button onClick={()=>{this.setState({del_dis:'none'})}} style={{backgroundColor:'lightgreen',}}>CANCEL</button>
                        <button style={{backgroundColor:'red',color:'white'}}>DELETE</button>
                    </div>    
                </div>
            </div>
        );
    }
}

export default withRouter(EditControllerComponent);
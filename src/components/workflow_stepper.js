import React from 'react';

// css imports
import '../css/home.css';
import workflow from '../pics/workflow.png';


class WorkflowstepperComponent extends React.Component{
    render(){
        return(
            <div className="top_header">
                <div className="content_card">
                    <div className="head_content">
                        Drag and drop widget to manage workflows.
                    </div>
                    <div className="description_content">
                       Customize workflow for each endpoint using pre-built widgets.
                       API Assistant parses your widget to high performance code behind the scenes.
                       Freeze the workflow and connect with any front-end application in minutes.
                    </div>
                </div>
                <div className="img_card">
                    <img src={workflow} alt="hello"/>
                </div>
            </div>
        );
    }
}

export default WorkflowstepperComponent;
import React from 'react';

import '../css/home.css';

class HomeheaderComponent extends React.Component{
    render(){
        return(
           <div className="home_header_div">
               <div className="home_header_title">
                   API Assistant
               </div>
               <div className="home_header_accounts">
                   <button>Signup</button>
                   <button>Login</button>
               </div>
           </div>
        );
    }
}


export default HomeheaderComponent;
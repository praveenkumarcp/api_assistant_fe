import React from 'react';
import axios from 'axios';

// css imports
import '../css/dashboard.css';
import ModelcardComponent from '../components/model_card';

class ModellistComponent extends React.Component{

    constructor(props){
        super(props);
        this.state = {modelList:[]}
    }

    componentDidMount(){
        var model_card_list = [];
        axios.get(`http://localhost:4000/api/v1/projects/${this.props.title}/models`, {
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        })
        .then(response=>{
            for(var i=0; i<response.data.models.length; i++){
                console.log(response.data.models[i]);
                model_card_list.push(<ModelcardComponent mname={response.data.models[i]} title={this.props.title}/>)
            }
            this.setState({modelList: model_card_list});
        })
    }

    render(){
        return(
            <div className="project_list_div">
                {this.props.title} project models
                <div className="project_list_section">
                    {this.state.modelList}
                </div>
            </div>
        );
    }
}

export default ModellistComponent;
import React from 'react';
import axios from 'axios';
import '../css/workflow.css';

class SecurityComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {permissions:{}, permission_widgets:[],authentication:undefined, get_auth:undefined, post_auth:undefined,
    put_auth:undefined, delete_auth:undefined};
    }
    componentDidMount(){
        const url = `http://localhost:4000/api/v1/permissions/${this.props.pname}/permissions`;
        axios.get(url,{
            headers:{
                'Authorization': localStorage.getItem('token')
            }
        })
        .then(response=>{
            this.setState({permissions:response.data});
        })
        .catch(err=>{console.log(err)});

        const fetch_permission_url = `http://localhost:4000/api/v1/permissions/${this.props.pname}/controllers/${this.props.cname}`;
        axios.get(fetch_permission_url,{
            headers:{
                'Authorization': localStorage.getItem('token')
            }
        })
        .then(response=>{
            this.setState({
                authentication: response.data.authentication,
                post_auth: response.data.post,
                get_auth:response.data.get,
                put_auth: response.data.put,
                delete_auth: response.data.delete
            });
        })
        .catch(err=>{console.log(err)});
    }
    createOptions = ()=>{
        var permissions_obj = this.state.permissions;
        var keys = Object.keys(permissions_obj);
        var options_arr = [];
        for(var i=0;i<keys.length;i++){
            options_arr.push(
                <option key={i}>{keys[i]}</option>
            );
        }
        return options_arr;
    }
    
    handleValueChange = ({ target }) => {
        if(target.name === "authentication"){
            this.setState({authentication: this.state.authentication === true ? false : true});
        }
        else{
            this.setState({[target.name]:target.value});
        }
    };
    handleSubmit = ()=>{
        var body = {
            authentication: this.state.authentication,
            post: this.state.post_auth,
            get: this.state.get_auth,
            put: this.state.put_auth,
            delete: this.state.delete_auth
        }
        const fetch_permission_url = `http://localhost:4000/api/v1/permissions/${this.props.pname}/controllers/${this.props.cname}`;
        axios.put(fetch_permission_url,body,{
            headers:{
                'Authorization': localStorage.getItem('token')
            }
        })
        .then(response=>{
            console.log(response.data);
        })
        .catch(err=>{console.log(err)});
    }
    render(){
        return(
            <div className="security_div">
                <div>
                    <div>Authentication</div>
                    <input onChange={this.handleValueChange} name="authentication" checked={this.state.authentication} type="checkbox"></input><p style={{display:'inline'}}> Add JWT Authentication 
                    security for this endpoint</p>
                </div>
                <br></br>
                <div>
                    <div>Authorization</div>
                    <div className="auth_div">
                        <table cellSpacing="20">
                            <tbody>
                            <tr>
                                <td>GET</td>
                                <td>
                                    <select onChange={this.handleValueChange} name="get_auth" value={this.state.get_auth}>
                                        {this.createOptions()}
                                    </select>
                                </td>    
                            </tr>
                            <tr>
                                <td>POST</td>
                                <td>
                                    <select onChange={this.handleValueChange}  name="post_auth" value={this.state.post_auth}> 
                                        {this.createOptions()}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>PUT</td>
                                <td>
                                    <select onChange={this.handleValueChange}  name="put_auth" value={this.state.put_auth}>
                                        {this.createOptions()}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>DELETE</td>
                                <td>
                                    <select onChange={this.handleValueChange}  name="delete_auth" value={this.state.delete_auth}>
                                        {this.createOptions()}
                                    </select>
                                </td>
                            </tr>                            
                            </tbody>
                        </table>
                    </div>
                </div>
                <button onClick={this.handleSubmit}>SAVE PERMISSIONS</button>    
            </div>
        );
    }
}

export default SecurityComponent;
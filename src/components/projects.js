import React from 'react'

// css imports 
import '../css/dashboard.css'
import NewprojectComponent from '../components/newProject';
import ProjectlistComponent from '../components/projectList';


class ProjectComponent extends React.Component{
    render(){
        return(
            <div className="project_list">
                <NewprojectComponent/>
                <ProjectlistComponent/>
            </div>
        );
    }
}

export default ProjectComponent;
import React from 'react';
import {withRouter} from 'react-router-dom';
import axios from 'axios';
// css imports
import '../css/accounts.css';

class SignupComponent extends React.Component{

    constructor(props){
        super(props);
        this.state = {username:"", password:"", email:"", uname_err_dis: 'none', pass_err_dis: 'none',
        mail_err_dis: 'none'}
        this.handleValueChange = this.handleValueChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleValueChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    };

    
    handleSubmit = async(event)=>{

        this.setState({mail_err_dis: 'none'});

        // username validation
        if(this.state.username.length < 8)
        {
            this.setState({uname_err_dis:'block'});
            return;
        }
        else
        {
            this.setState({uname_err_dis:'none'});
        }

        // password validation    
        if(this.state.password.length < 8)
        {
            this.setState({pass_err_dis:'block'});
            return;
        }
        else
        {
            this.setState({pass_err_dis:'none'});
        }

        const signup_url = 'http://localhost:4000/api/v1/accounts/signup';
        var json_body={
            username: this.state.username,
            password: this.state.password,
            email: this.state.email
        };
        try{
            var response = await axios.post(signup_url,json_body);
            if(response.status === 201){
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('username', response.data.account.username);
                this.props.history.push('/dashboard');
            }
            else if(response.status === 200){
                this.setState({mail_err_dis: 'block'});
            }
        }
        catch(err){
           console.log("error is",err);
        }
    }
    render(){
        return(
            <div className="form_card">
                <center>
                    <h3>
                        <b>SIGNUP</b>
                    </h3>
                    <input name="username" onChange={this.handleValueChange} value={this.state.username} type="text" placeholder="username" autoComplete="off" autoFocus/>
                    <p style={{display:this.state.uname_err_dis}}>username must be minimum 8 characters</p>
                    <input name="email" onChange={this.handleValueChange}  value={this.state.email} type="email" placeholder="email" autoComplete="off"/>
                    <p style={{display:this.state.mail_err_dis}}>email id already exist</p>
                    <input name="password" onChange={this.handleValueChange}  value={this.state.password} type="password" placeholder="password" autoComplete="off"/>
                    <p style={{display:this.state.pass_err_dis}}>password must be minimum 8 characters</p>
                    <button onClick={this.handleSubmit}>
                            <b>CREATE ACCOUNT</b>
                    </button>
                    <div className="accounts_link">
                        Already have an account ? &nbsp;
                        <a href='/login'>Signin here</a>
                    </div>
                </center>
            </div>
        );
    }
}

export default withRouter(SignupComponent);
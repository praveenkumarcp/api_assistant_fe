import React from 'react';
import axios from 'axios';

import '../css/controller.css';
import '../css/dashboard.css';

class NewControllerComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {endpoint:'', cname:''};
    }
    handleValueChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    };
    handleSubmit = async(e)=>{
        e.preventDefault();
        var url = `http://localhost:4000/api/v1/controllers/${this.props.pname}/controllers`;
        axios.post(url, this.state, {
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        })
        .then(response=>console.log(response.data))
        .catch(err=>console.log(err));
        this.setState({cname:'', endpoint:''});
    }

    render(){
        return(
           <div className="new_controller">
               New Controller
               <br></br>
               <form>
               <input name="cname" onChange={this.handleValueChange} value={this.state.cname} placeholder="Title" size={13} autoComplete="off" autoFocus></input>
               <input style={{marginLeft:'20px'}} name="endpoint" onChange={this.handleValueChange} value={this.state.endpoint} placeholder="Endpoint " size={50} autoComplete="off"></input>
               <button onClick={this.handleSubmit}>CREATE</button>
               </form>
           </div>
        );
    }
}

export default NewControllerComponent;
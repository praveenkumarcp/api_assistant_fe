import React from 'react';

// css imports
import '../css/accounts.css';

class BannerComponent extends React.Component{
    render(){
        return(
            <div>
                <div class="banner_header">
                    <b>
                     Signup for free. 
                    </b>
                </div>
                <div className="banner_content">
                    API Assistant is a platform to develop REST APIs in minutes with drag and drop feature.
                    Fastest request-response cycle ever with customized workflows. Design your models 
                    and connect with any front-end application in minutes.
                </div>
            </div>
        );
    }
}

export default BannerComponent;
import React from 'react';
import axios from 'axios';

import '../css/switch.css'

class ServercardComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {server: "unchecked", url:""}
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e){
        var url;
        if(this.state.server === "unchecked"){
            this.setState({server:"checked"});
            console.log("starting server");
            url = `http://localhost:4000/api/v1/be_projects/projects/${this.props.pname}/server`;
            axios.get(url,{
                headers:{
                    'Authorization': localStorage.getItem('token')
                }
            })
            .then(response=>{
                var server_url = `localhost:${response.data.port}`;
                this.setState({url: server_url});
                console.log(response.data);
            })
            .catch(err=>{
                console.log("err",err);
            })
        }
        else{
            this.setState({server:"unchecked"});
            console.log("stopping server");
            url = `http://localhost:4000/api/v1/be_projects/projects/${this.props.pname}/server`;
            axios.post(url,null,{
                headers:{
                    'Authorization': localStorage.getItem('token')
                }
            })
            .then(response=>{
                this.setState({url:""})
                console.log(response.data);
            })
            .catch(err=>{
                console.log("err",err);
            })
        }
    }

    render(){
        return(
                <div className="switch_div">
                    <i style={{marginRight:'5%',fontSize:'140px',color:'yellow', }} className="material-icons">folder</i>
                    <br></br>
                    <div style={{textAlign:'center', width:'50%'}}>
                        {this.props.pname}
                    </div>
                    <br></br><br></br>
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <div style={{display:'inline',margin:'5px',}}>Deploy</div>
                            </td>
                            <td>
                                <label style={{marginRight:'25%'}} className="switch">
                                <input onChange={this.handleChange} type="checkbox"/>
                                <span className="slider round"></span>
                                </label>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    {this.state.url === "" ? <p style={{display:'inline'}}></p> : <a href={this.state.url} rel="noopener noreferrer" target="_blank">Test URL</a>}
                </div>
        );
    }
}


export default ServercardComponent;
import React from 'react';
import axios from 'axios';

// css imports
import '../css/dashboard.css';
import ProjectcardComponent from '../components/project_card';

class ProjectlistComponent extends React.Component{

    constructor(props){
        super(props);
        this.state = {projectList:[]}
    }

    componentDidMount(){
        var project_card_list = [];
        axios.get('http://localhost:4000/api/v1/be_projects/projects', {
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        })
        .then(response=>{
            for(var i=0; i<response.data.projects.length; i++){
                console.log(response.data.projects[i]);
                project_card_list.push(<ProjectcardComponent title={response.data.projects[i]}/>)
            }
            this.setState({projectList: project_card_list});
        })
    }

    render(){
        return(
            <div className="project_list_div">
                Your projects
                <div className="project_list_section">
                    {this.state.projectList}
                </div>
            </div>
        );
    }
}

export default ProjectlistComponent;
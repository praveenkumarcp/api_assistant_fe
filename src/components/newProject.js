import React from 'react';
import axios from 'axios';

class NewprojectComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {title:'', err_msg:''}
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleValueChange = this.handleValueChange.bind(this);
    }
    async handleSubmit(e){
        this.setState({title: this.state.title.toLowerCase()});
        this.setState({err_msg:''})
        e.preventDefault();
        
        // checking for duplicate 
        var project_list_response = await axios.get('http://localhost:4000/api/v1/be_projects/projects',{
            headers: {
                'Authorization': localStorage.getItem('token'),
            }
        });
        var project_list = project_list_response.data.projects;
        if(project_list.indexOf(this.state.title) !== -1)
        {
                this.setState({err_msg:'project name already exist'});
                return;
        }
        var json_body = {folder_name: this.state.title};
        axios.post('http://localhost:4000/api/v1/be_projects/projects', json_body, {
            headers: {
                'Authorization': localStorage.getItem('token')
            }
        })
        .then(response=>console.log(response.data))
        .catch(err=>console.log(err));
        this.setState({title:''});
    }
    handleValueChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
        var title_length = this.state.title.length;
        if(title_length === 1)
            this.setState({err_msg:''});
    };
    render(){
        return(
            <div className="new_project_div">
                New Project
                <br></br>
                <form >
                <input value={this.state.title} onChange={this.handleValueChange} name="title" placeholder="Title" autoFocus autoComplete="off"/>
                <button onClick={this.handleSubmit}>CREATE</button>
                <p>{this.state.err_msg}</p>
                </form>
            </div>
        );
    }
}

export default NewprojectComponent;
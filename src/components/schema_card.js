import React from 'react';
import {withRouter} from 'react-router-dom';
import axios from 'axios';

// custom imports
import '../css/schema.css';

class SchemacardComponent extends React.Component{  

    constructor(props){
        super(props);
        this.state = {columnData: [], cards: []};
        this.handleValueChange = this.handleValueChange.bind(this);
        this.addNewCard = this.addNewCard.bind(this);
        this.createIDCard = this.createIDCard.bind(this);
        this.freezeSchema = this.freezeSchema.bind(this);
    }

    async componentDidMount(){
        var response = await axios.get(`http://localhost:4000/api/v1/projects/${this.props.title}/models/${this.props.mname}/schema`, {
            headers: {
                'Authorization': localStorage.getItem('token'),
            }
        })
        this.setState({columnData: response.data.columns});
        this.renderCards();
    }

    renderCards(){
        var card_array = [];
        card_array.push(this.createIDCard());
        for(var i=0; i<this.state.columnData.length; i++){
            card_array.push(this.createCard(i));
        }
        this.setState({cards: card_array});
    }

    handleValueChange(e, i, type){
        var all_columns = this.state.columnData;
        var specific_column = all_columns[i];
        if(type === "name")
            specific_column.col_name = e.target.value;
        if(type === "type")
            specific_column.type = e.target.value;
        if(type === "null")
            specific_column.constraints.null = specific_column.constraints.null === true ? false: true;
        if(type === "unique")
            specific_column.constraints.unique = specific_column.constraints.unique === true ? false: true;
        all_columns[i] = specific_column
        this.setState({columnData: all_columns});
        console.log(this.state.columnData);
        this.renderCards();
    }

    createCard(i){
        return(
            <div className="schema_card">
                <div className="schema_card_head">
                    <center>
                        <input onChange={(e)=>this.handleValueChange(e,i, "name")} value={this.state.columnData[i].col_name}  type="text" placeholder="Column name"/>
                        <select onChange={(e)=>this.handleValueChange(e,i, "type")} value={this.state.columnData[i].type}>
                            <option value="Sequelize.INTEGER">INT</option>
                            <option value="Sequelize.STRING">STRING</option>
                            <option value="Sequelize.FLOAT">FLOAT</option>
                        </select>
                    </center>    
                    <hr></hr>                    
                </div>
                <div className="schema_card_body">
                    <input onChange={(e)=>this.handleValueChange(e,i, "null")} checked={this.state.columnData[i].constraints.null} type="checkbox" />NULL
                    <input onChange={(e)=>this.handleValueChange(e,i, "unique")} checked={this.state.columnData[i].constraints.unique} type="checkbox" />UNIQUE
                </div>
            </div>
        );
    }

    addNewCard(){
        var new_column = {
            col_name : "column"+(this.state.columnData.length+2),
            type : "Sequelize.STRING",
            constraints: {
                null: false,
                unique: false
            }
        }
        var all_columns = this.state.columnData;
        all_columns.push(new_column);
        this.setState({columnData: all_columns});
        this.renderCards();
    }

    createIDCard(){
        return(
            <div className="schema_card">
            <div className="schema_card_head">
                <center>
                    <input contentEditable={false} value="id" type="text" />
                    <select contentEditable={false} value="INT">
                        <option value="INT">INT</option>
                        <option value="STRING">STRING</option>
                        <option value="FLOAT">FLOAT</option>
                    </select>
                </center>    
                <hr></hr>                    
            </div>
            <div style={{paddingBottom:'4%'}} className="schema_card_body">
                <input contentEditable={false} checked={true} type="checkbox" />PRIMARY KEY
            </div>
        </div>
        );
    }

    async freezeSchema(){
        const put_url = `http://localhost:4000/api/v1/projects/${this.props.title}/models/${this.props.mname}/schema`;
        var json_body = {
            columns: this.state.columnData
        }
        var response = await axios.put(put_url,json_body, {
            headers:{
                'Authorization': localStorage.getItem('token')
            }
        });
        console.log(response.data);
    }

    render(){
        return(
            <div>
                {this.state.cards}
                <div onClick={this.addNewCard} className="add_card">
                    <i class="material-icons">add_box</i>
                </div>
                <div className="freeze_button">
                    <button onClick={this.freezeSchema}>Freeze Schema</button>
                </div>
             </div>
        )
    }
}



export default withRouter(SchemacardComponent);
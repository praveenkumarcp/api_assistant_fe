import React from 'react';
import {withRouter} from 'react-router-dom';

// css imports
import '../css/projects.css';

class ModelcardComponent extends React.Component{
 
    render(){
        return(
            <div onClick={()=>{
                this.props.history.push(`/projects/${this.props.title}/models/${this.props.mname}`)
            }} className="model_card">
                <i class="material-icons">storage</i>
                <center>
                    {this.props.mname}
                </center>
            </div>
        )
    }
}

export default withRouter(ModelcardComponent);
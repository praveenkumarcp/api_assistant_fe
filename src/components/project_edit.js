import React from 'react';

import '../css/projects.css';

class ProjecteditComponent extends React.Component{
    constructor(props){
        super(props);
        this.state={show_del:'none'}
        this.handleShow = this.handleShow.bind(this);
    }
    handleShow(){
        this.setState({show_del: this.state.show_del === 'none' ? 'block': 'none'});
    }
    render(){
        return(
            <div className="project_edit_div">
                Project Name
                <br></br>
                <input style={{marginTop:'2.7%'}} value={this.props.pname}/>
                <button style={{marginLeft:'3%'}}>CHANGE NAME</button>
                <br></br>
                <div className="mc_div">
                    Models and Controllers
                    <br></br>
                    <div className="mc_detail">
                    <i class="material-icons">storage</i>
                    <div>5 Models</div>
                    </div> 
                    <div className="mc_detail">
                    <i class="material-icons">developer_board</i>
                    <div>5 Controllers</div>
                    </div> 
                </div>
                <div className="project_delete_div">
                    <p>Delete this project</p>
                    <table>
                        <tr style={{cursor:'grab'}} onClick={this.handleShow}>
                            <td>
                                <i className="material-icons">delete_forever</i>
                            </td>
                            <td>
                                Delete this project !
                            </td>
                        </tr>
                    </table>
                </div>
                <div style={{display:this.state.show_del}} className="delete_msg_div">
                        <p>Deleting the project will delete all the data associated with it !</p>
                        <button onClick={this.handleShow}>CANCEL</button>
                        <button style={{backgroundColor:'red', color:'white'}}>DELETE</button>
                </div>
            </div>
        );
    }
}


export default ProjecteditComponent;
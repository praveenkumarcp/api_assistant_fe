import React from 'react';
import {Link} from 'react-router-dom';
// css imports
import '../css/dashboard.css';
import ServercardComponent from '../components/server_card';

class ProjectsidebarComponent extends React.Component{

    render(){
        return(
            <div>
                <div className="side_bar">
                    <div className="server_div">
                        <ServercardComponent pname={this.props.pname}/>     
                    </div>
                    <Link to="/dashboard">Back to dashboard</Link> 
                    <Link to={`/projects/${this.props.pname}/home`}>Home</Link> 
                    <Link to={`/projects/${this.props.pname}/models`}>Models</Link> 
                    <Link to={`/projects/${this.props.pname}/controllers`}>Controllers</Link> 
                </div>                 
              

            </div>
            
        );
    }
}

export default ProjectsidebarComponent;
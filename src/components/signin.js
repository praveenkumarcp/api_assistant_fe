import React from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom';

// css imports
import '../css/accounts.css';


class SigninComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {email:'user1@mail.com', password: 'Praveencp6;', err_msg: ''};
        this.handleValueChange = this.handleValueChange.bind(this);    
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleValueChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    };

    handleSubmit = async(event)=>{
        this.setState({err_msg:''});
        event.preventDefault();
        const login_url = 'http://localhost:4000/api/v1/accounts/login';
        var json_body={
            email: this.state.email,
            password: this.state.password,
        };
        try{
            var response = await axios.post(login_url,json_body);
            if(response.status === 201){
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('username', response.data.username);
                this.props.history.push('/dashboard');
            }
            else{
                this.setState({err_msg:'Invalid Login credentials', email:'', password:''});
            }
        }
        catch(err){
            console.log(err);
        }
    }

    render(){
        return(
            <div className="form_card">
                <center>
                    <h3>
                        <b>SIGNIN</b>
                    </h3>
                    <form onSubmit={this.handleSubmit}>
                    <input name="email" value={this.state.email} onChange={this.handleValueChange} type="email" placeholder="email" autoFocus autoComplete="off"/>
                    <input name="password" value={this.state.password} onChange={this.handleValueChange} type="password" placeholder="password"/>
                    <button type="submit">
                            <b>LOGIN</b>
                    </button>
                    </form>
                    <p>{this.state.err_msg}</p>
                    <div className="accounts_link">
                        New member ? &nbsp;
                        <a href='/signup'>Signup here</a>
                    </div>
                </center>
            </div>
        );
    }
}

export default withRouter(SigninComponent);
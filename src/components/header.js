import React from 'react';

// css imports
import '../css/dashboard.css';

class HeaderComponent extends React.Component{
    render(){
        return(
            <div>
                <div className="title_div">
                    API Assistant
                </div>
                <div className="username_div">
                    Hello {localStorage.getItem('username')}
                </div>
            </div>
        );
    }
}

export default HeaderComponent;
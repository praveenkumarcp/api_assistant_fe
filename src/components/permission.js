import React from 'react';
import axios from 'axios';

import '../css/workflow.css';

class PermissionComponent extends React.Component{
    constructor(props){
        super(props);
        this.state = {permissions:{}, permission_widgets:[], perm_name:undefined};
    }
    componentDidMount(){
        const url = `http://localhost:4000/api/v1/permissions/${this.props.pname}/permissions`;
        axios.get(url,{
            headers:{
                'Authorization': localStorage.getItem('token')
            }
        })
        .then(response=>{
            this.setState({permissions:response.data});
            this.createPermissionWidgets();
        })
        .catch(err=>{console.log(err)});
    }
    createPermissionWidgets = ()=>{
        var widget_arr = [];
        var keys = Object.keys(this.state.permissions);
        for(var i=0;i<keys.length;i++){
            widget_arr.push(
                <tr>
                    <td>{keys[i]}</td>
                    <td>level - {this.state.permissions[keys[i]]}</td>
                    <td>
                        <i class="material-icons">delete</i>
                    </td>
                </tr>
            );
        }
        this.setState({permission_widgets:widget_arr});
    }
    handleChange = (event)=>{
        this.setState({perm_name:event.target.value});
    }
    handleSubmit = ()=>{
        const url = `http://localhost:4000/api/v1/permissions/${this.props.pname}/permissions`;
        var body = {"perm_name":this.state.perm_name};
        axios.post(url,body,{
            headers:{
                'Authorization': localStorage.getItem('token')
            }
        })
        .then(response=>{
            this.setState({permissions:response.data});
            this.setState({perm_name:undefined})
            this.createPermissionWidgets();
        })
        .catch(err=>{console.log(err)});
    }
    render(){
        return(
            <div className="perm_div">
                <div>Permissions</div>
                <input onChange={this.handleChange} value={this.state.perm_name}></input>
                <button onClick={this.handleSubmit}>ADD</button>
                <div className="perm_list_div">
                    <div>{this.props.pname} project permissions</div>
                    <table cellspacing="40">
                        {this.state.permission_widgets}
                    </table>
                </div>
                
            </div>
        );
    }
}

export default PermissionComponent;
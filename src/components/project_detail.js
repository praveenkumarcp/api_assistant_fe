import React from 'react';

import '../css/projects.css';

class ProjectdetailComponent extends React.Component{

    render(){
        return(
            <div className="project_detail_div">
                <center>
                    <i class="material-icons">folder</i>
                    <br></br>
                    {this.props.pname}
                </center>
            </div>
        )
    }
}

export default ProjectdetailComponent;
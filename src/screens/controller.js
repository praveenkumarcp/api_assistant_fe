import React from 'react';
import {withRouter} from 'react-router-dom';

import '../css/dashboard.css';
import HeaderComponent from '../components/header';
import ProjectsidebarComponent from '../components/project_sidebar';
import NewControllerComponent from '../components/new_controller';
import AllcontrollersComponent from '../components/allControllers';

class ControllerScreen extends React.Component{
    render(){
        return(
            <div className="api_div">
                <div className="header_div">
                    <HeaderComponent/>
                </div>   
                <ProjectsidebarComponent pname={this.props.match.params.projectname}/> 
                <div>
                <NewControllerComponent pname={this.props.match.params.projectname}/>
                <AllcontrollersComponent pname={this.props.match.params.projectname}/>
                </div>
                
            </div>
        )
    }
}

export default withRouter(ControllerScreen);
import React from 'react';

// component imports
import SidebarComponent from '../components/sidebar';
import ProjectComponent from '../components/projects';
import HeaderComponent from '../components/header';
import {withRouter} from 'react-router-dom';

// css imports
import '../css/dashboard.css';

class DashboardScreen extends React.Component{
    render(){
        return(
            <div className="api_div">
                <div className="header_div">
                    <HeaderComponent/>
                </div>
                <SidebarComponent/>
                <ProjectComponent/>
            </div>
            
        );
    }
}

export default withRouter(DashboardScreen);
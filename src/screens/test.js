import React from 'react';

import '../css/test.css';


class TestScreen extends React.Component{
    constructor(props){
        super(props);
        this.state={body:''}
    }
    handleValue=()=> {

    }
    render(){
        return(
            <div className="test_div">
                <div className="header_div">
                    <div>API Assistant Testing Console</div>
                </div>
                <div className="test_input">
                    <select>
                        <option>POST</option>
                        <option>GET</option>
                        <option>PUT</option>
                        <option>DELETE</option>
                    </select>
                    <input></input>
                    <button>SEND</button>
                </div>
                <div className="req_sec">
                <textarea placeholder="Request Body" rows="30" cols="70"></textarea>
                </div>
                <div className="res_sec">
                <textarea placeholder="Response Body" rows="30" cols="70"></textarea>
                </div>
            </div>
        );
    }
}

export default TestScreen;
import React from 'react';
import {withRouter} from 'react-router-dom';

// custom import 
import '../css/dashboard.css';
import '../css/projects.css';
import HeaderComponent from '../components/header';
import ProjectsidebarComponent from '../components/project_sidebar';
import ModelsectionComponent from '../components/model_section';

class ProjectmodelScreen extends React.Component{
    render(){
        return(
            <div className="api_div">
                <div className="header_div">
                    <HeaderComponent/>
                </div>
                <ProjectsidebarComponent pname={this.props.match.params.projectname}/>
                <ModelsectionComponent title={this.props.match.params.projectname}/>
            </div>
        );
    }
}

export default withRouter(ProjectmodelScreen);
import React from 'react';

// component imports
import BannerComponent from '../components/banner';
import SignupComponent from '../components/signup';

// css imports
import '../css/accounts.css';

class SignupScreen extends React.Component{
    render(){
        return(
            <div className="accounts_div">
                <center>
                    <h1>API Assistant</h1>
                </center>
                <div className="banner_card">
                    <BannerComponent/>
                </div>
                <div className="signup_card">
                    <SignupComponent/>
                </div>
            </div>
        );
    }
}

export default SignupScreen;
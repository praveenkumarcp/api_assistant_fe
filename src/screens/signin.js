import React from 'react';

// component imports
import BannerComponent from '../components/banner';
import SigninComponent from '../components/signin';

// css imports
import '../css/accounts.css';

class SigninScreen extends React.Component{
    render(){
        return(
            <div className="accounts_div">
                <center>
                    <h1>API Assistant</h1>
                </center>
                <div className="banner_card">
                    <BannerComponent/>
                </div>
                <div className="signin_card">
                    <SigninComponent/>
                </div>
            </div>
        );
    }
}

export default SigninScreen;
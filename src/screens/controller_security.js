import React from 'react';
import {withRouter} from 'react-router-dom';

// css imports
import '../css/dashboard.css';
import HeaderComponent from '../components/header';
import ProjectsidebarComponent from '../components/project_sidebar';
import SecurityComponent from '../components/security';


class ControllersecurityScreen extends React.Component{
    render(){
        return(
            <div className="api_div">
                <div className="header_div">
                    <HeaderComponent/>
                </div>
                <ProjectsidebarComponent pname={this.props.match.params.projectname}/>
                <SecurityComponent pname={this.props.match.params.projectname} cname={this.props.match.params.cname}/>
            </div>
        )
    }
}

export default withRouter(ControllersecurityScreen);

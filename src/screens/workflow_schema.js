import React from 'react';
import axios from 'axios';

import '../css/dashboard.css';
import '../css/workflow.css';
import HeaderComponent from '../components/header';

class WorkflowschemaScreen extends React.Component{
    constructor(props){
        super(props);
        this.state={widgets:[],widget_json:[], response_json:[], response_widgets:[], status:undefined};
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.newResponseInput = this.newResponseInput.bind(this);
        this.newResponseVar = this.newResponseVar.bind(this);
        this.newVariable = this.newVariable.bind(this);
        this.removeWidget = this.removeWidget.bind(this);
        this.formResponseWidgets = this.formResponseWidgets.bind(this);
    }
    componentDidMount(){
        var pname = this.props.match.params.projectname;
        var method = this.props.match.params.option;
        const url = `http://localhost:4000/api/v1/controllers/${pname}/controllers/${this.props.match.params.cname}?method=${method}`;
        axios.get(url, {
            headers:{
                'Authorization':localStorage.getItem('token')
            }
        })
        .then(response=>{
            this.setState({status:response.data.status,widget_json: response.data.workflow, response_json: response.data.response});
            this.formWorkflowWidgets();
            this.formResponseWidgets();
        })
        .catch(err=>console.log(err));
    }
    formWorkflowWidgets = ()=>{
        var widget_arr = [];
        for(var i=0;i<this.state.widget_json.length;i++){
            var key = i;
            var curr_obj = this.state.widget_json[i];
            switch(Object.keys(curr_obj)[0]){
                default:
                    break;
                case 'e000':
                    widget_arr.push(
                        <div key={key} className="ind_widget">
                            name of the variable 
                            <input onChange={(event)=>{this.handleChange(event,key,"e000","name")}}  value={this.state.widget_json[key].e000.name}/> 
                            and its value is 
                            <input onChange={(event)=>{this.handleChange(event,key,"e000","value")}} value={this.state.widget_json[key].e000.value}/>
                            {this.closeWidget(key)}
                        </div> 
                    );
                    break;
                case 'f000':
                    widget_arr.push(
                        <div key={key} className="ind_widget">
                        Fetch all the rows from table
                        <input onChange={(event)=>{this.handleChange(event,key,"f000","table_name")}} 
                        value={this.state.widget_json[key].f000.table_name}/> 
                        and store in variable
                        <input onChange={(event)=>{this.handleChange(event,key,"f000","var_name")}} 
                    value={this.state.widget_json[key].f000.var_name}/>
                        {this.closeWidget(key)}
                    </div>    
                    );
                    break;
            }
        }
        this.setState({widgets: widget_arr});
        
    }
    formResponseWidgets = ()=>{
        var resp_widget_arr = [];
        for(var i=0;i<this.state.response_json.length;i++){
            var key = i;
            resp_widget_arr.push(
                <div className="resp_input" key={key}>
                    <input placeholder="key" onChange={(e)=>{this.handleResponseChange(e,key,"key")}}  value={this.state.response_json[key]['key']}/>
                    <input placeholder="value" onChange={(e)=>{this.handleResponseChange(e,key,"value")}}  value={this.state.response_json[key]['value']}/>
                </div>
            );
        }
        this.setState({response_widgets: resp_widget_arr});
    }
    handleClick(fun){
        this.setState({widgets:this.state.widgets.concat(fun(this.state.widgets.length))});
    }
    removeWidget = (i)=>{
        console.log(i);
    }
    handleChange = (event,i,type, field)=>{
        var json_array = this.state.widget_json;
        json_array[i][type][field] = event.target.value; 
        this.setState({widget_json:json_array});
        console.log(this.state.widget_json);
        this.formWorkflowWidgets();
    }
    closeWidget = (i)=>{
        return(
            <div className="close_widget">
                <i onClick={()=>{this.removeWidget(i)}} className="material-icons">delete</i>
            </div>
        );
    }
    newVariable = (key)=>{
            var json =  {name:undefined, value:undefined};
            var obj = {"e000": json};
            var json_array = this.state.widget_json;
            json_array.push(obj);
            this.setState({widget_json:json_array});
            return(
                <div key={key} className="ind_widget">
                    name of the variable 
                    <input onChange={(event)=>{this.handleChange(event,key,"e000","name")}} 
                    value={this.state.widget_json[key].e000.name}/> and its value is
                    <input placeholder="undefined" onChange={(event)=>{this.handleChange(event,key,"e000","value")}} 
                    value={this.state.widget_json[key].e000.value}/> 
                    {this.closeWidget(key)}
                </div>           
            );
    }
    fetchData = (key)=>{
        var json =  {table_name:undefined,};
        var obj = {"f000": json};
        var json_array = this.state.widget_json;
        json_array.push(obj);
        this.setState({widget_json:json_array});
        return(
            <div key={key} className="ind_widget">
                Fetch all the rows from table
                <input onChange={(event)=>{this.handleChange(event,key,"f000","table_name")}} 
                value={this.state.widget_json[key].f000.table_name}/> 
                and store in variable
                <input onChange={(event)=>{this.handleChange(event,key,"f000","var_name")}} 
                    value={this.state.widget_json[key].f000.var_name}/>
                {this.closeWidget(key)}
            </div>           
        );
}

    handleStatusChange = (event) =>{
        this.setState({status: event.target.value});
    }
    handleResponseChange = (event, i, tag)=>{
        var response_json = this.state.response_json;
        response_json[i][tag] = event.target.value;
        this.setState({response_json: response_json});
        this.formResponseWidgets();
    }
    newResponseInput = (key)=>{
        return(
            <div className="resp_input" key={key}>
                 <input placeholder="key" onChange={(e)=>{this.handleResponseChange(e,key,"key")}}  value={this.state.response_json[key]['key']}/>
                 <input placeholder="value" onChange={(e)=>{this.handleResponseChange(e,key,"value")}}  value={this.state.response_json[key]['value']}/>
            </div>
        );
    }
    newResponseVar = ()=>{
        var length = this.state.response_json.length;
        var resp_json = {key:undefined, value:undefined};
        var response_json_arr = this.state.response_json;
        response_json_arr.push(resp_json);
        this.setState({response_json: response_json_arr});
        var response_widgets = this.state.response_widgets;
        response_widgets = response_widgets.concat(this.newResponseInput(length));
        this.setState({response_widgets: response_widgets});
    }
    handleSubmit = async()=>{
        var resp_obj = {};
        resp_obj['status'] = this.state.status;
        resp_obj['workflow'] = this.state.widget_json;
        resp_obj['response'] = this.state.response_json;
        resp_obj['cname'] = this.props.match.params.cname;
        resp_obj['method'] = this.props.match.params.option;
        var pname = this.props.match.params.projectname;
        const url = `http://localhost:4000/api/v1/controllers/${pname}/controllers/${this.props.match.params.cname}`;
        var response = await axios.put(url, resp_obj, {
            headers:{
                'Authorization': localStorage.getItem('token')
            }
        });
        console.log(response.data);
    }
    render(){
        return(
            <div className="api_div">
                <div className="header_div">
                    <HeaderComponent/>
                </div>
                <div className="workflow_div">

                    <div className="widget_div"> 
                    Primitive widgets
                    <br></br>  
                        <div onClick={()=>{this.handleClick(this.newVariable)}} className="widget">
                            Intialize new variable       
                        </div>    
                    </div>

                    <div className="build_div">
                        Workflow
                        <br></br>
                        {this.state.widgets}
                        <hr></hr>
                        <div className="response_div">
                            Response
                            <br></br>
                            Status Code <input onChange={this.handleStatusChange} style={{width:'5%'}} value={this.state.status}/>
                            <br></br>
                            {this.state.response_widgets}
                            <i onClick={this.newResponseVar} style={{cursor:'grab'}} className="material-icons">add</i>
                            <hr></hr>
                            <button onClick={this.handleSubmit}>Freeze Workflow</button>

                        </div>
                    </div>


                    <div className="dao_div">
                        DAO widgets
                        <br></br>
                        <div onClick={()=>{this.handleClick(this.fetchData)}} className="widget">
                            Fetch data from table      
                        </div>   
                    </div>
                </div>
            </div>
        );
    }
}

export default WorkflowschemaScreen;
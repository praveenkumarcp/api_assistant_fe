import React from 'react';

// component imports
 import IntrostepperComponent from '../components/intro_stepper';
 import ModelstepperComponent from '../components/model_stepper';
 import WorkflowstepperComponent from '../components/workflow_stepper';
 import DeploystepperComponent from '../components/deploy_stepper';
 import HomeheaderComponent from '../components/home_header';

class HomeScreen extends React.Component{
    render(){
        return(
            <div>
                <HomeheaderComponent/>
                <IntrostepperComponent/>
                <ModelstepperComponent/>
                <WorkflowstepperComponent/>
                <DeploystepperComponent/>
            </div>
        );
    }
}

export default HomeScreen;
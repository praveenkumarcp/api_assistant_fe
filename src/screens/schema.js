import React from 'react';
import {withRouter} from 'react-router-dom';

// custom imports
import SchemacardComponent from '../components/schema_card';
import HeaderComponent from '../components/header';
import SchemanavComponent from '../components/schema_nav';

import '../css/schema.css';
import '../css/dashboard.css';

class SchemaScreen extends React.Component{
    
    render(){   
        return(
            <div className="api_div">
                <div className="header_div">
                    <HeaderComponent/>
                    <br></br>
                    <SchemanavComponent pname={this.props.match.params.projectname} 
                        mname={this.props.match.params.mname} />
                <div className="schema_section">
                    <SchemacardComponent title={this.props.match.params.projectname} 
                        mname={this.props.match.params.mname}/>
                </div>
                </div>
            </div>
                
        );
    }
}


export default withRouter(SchemaScreen);
import React from 'react';

import '../css/dashboard.css';

import HeaderComponent from '../components/header';
import ProjectsidebarComponent from '../components/project_sidebar';
import PermissionComponent from '../components/permission';

class PermissionScreen extends React.Component{
    render(){
        return(
            <div className="api_div">
                <div className="header_div">
                    <HeaderComponent/>
                </div>
                <ProjectsidebarComponent pname={this.props.match.params.projectname}/>
                <PermissionComponent pname={this.props.match.params.projectname}/>
            </div>
        )
    }
}

export default PermissionScreen;
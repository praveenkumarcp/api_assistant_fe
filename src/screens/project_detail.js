import React from 'react';
import {withRouter} from 'react-router-dom';

// css imports
import '../css/dashboard.css';
import HeaderComponent from '../components/header';
import ProjectsidebarComponent from '../components/project_sidebar';
import ProjecteditComponent from '../components/project_edit';
import ProjectdetailComponent from '../components/project_detail';

class ProjectdetailScreen extends React.Component{
    render(){
        return(
            <div className="api_div">
                <div className="header_div">
                    <HeaderComponent/>
                </div>
                <ProjectsidebarComponent pname={this.props.match.params.projectname}/>
                <ProjecteditComponent pname={this.props.match.params.projectname}/>
                <ProjectdetailComponent pname={this.props.match.params.projectname}/>
            </div>
        )
    }
}

export default withRouter(ProjectdetailScreen);

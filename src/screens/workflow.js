import React from 'react';

import '../css/dashboard.css';
import HeaderComponent from '../components/header';
import ProjectsidebarComponent from '../components/project_sidebar';
import EditControllerComponent from '../components/editController';

class WorkflowScreen extends React.Component{
    render(){
        return(
            <div className="api_div">
                <div className="header_div">
                        <HeaderComponent/>
                </div>
            <ProjectsidebarComponent pname={this.props.match.params.projectname}/>
            <EditControllerComponent pname={this.props.match.params.projectname}
             cname={this.props.match.params.cname}/>
            </div>
        );
    }
}

export default WorkflowScreen;